#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>


typedef struct {
    bool Initialized;

    int Size;
    int * V;
    int * Result;

    pthread_mutex_t * Lock;
    pthread_cond_t * Cond;
} Context;


void * vec_norm(void * p_context) {
    Context * context = (Context *) p_context;

    time_t start = time(NULL);

    printf("[vec_norm:%llds] checking\n", time(NULL) - start);
    fflush(stdout);

    pthread_mutex_lock(context->Lock);
    while (false == context->Initialized) {
        printf("[vec_norm:%llds] not initialized, waiting\n", time(NULL) - start);
        fflush(stdout);

        pthread_cond_wait(context->Cond, context->Lock);
    }
    pthread_mutex_unlock(context->Lock);

    printf("[vec_norm:%llds] initialized\n", time(NULL) - start);
    fflush(stdout);

    int sum_of_squares = 0;
    for (int i = 0; i < context->Size; i += 1) {
        sum_of_squares += context->V[i] * context->V[i];
    }

    *context->Result = (int) sqrt((double) sum_of_squares);

    return NULL;
}


void vec_fill(
        int * vec,
        int size,
        int value) {
    for (int i = 0; i < size; i += 1) {
        vec[i] = value;
    }
}


int main() {
    const int size = 100;

    int v[size];
    int result = 0;

    pthread_mutex_t mutex;
    pthread_mutexattr_t mutexattrs;

    pthread_mutexattr_init(&mutexattrs);
    pthread_mutex_init(&mutex, &mutexattrs);
    pthread_mutexattr_destroy(&mutexattrs);

    pthread_cond_t cond;
    pthread_condattr_t condattrs;

    pthread_condattr_init(&condattrs);
    pthread_cond_init(&cond, &condattrs);
    pthread_condattr_destroy(&condattrs);

    Context context = (Context) {
            .Initialized = false,
            .Size = size,
            .V = v,
            .Result = &result,
            .Lock = &mutex,
            .Cond = &cond,
    };

    pthread_t worker;
    pthread_attr_t attrs;

    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);

    pthread_create(&worker, &attrs, vec_norm, &context);
    pthread_attr_destroy(&attrs);

    sleep(5);
    vec_fill(v, size, 5);
    pthread_mutex_lock(&mutex);
    context.Initialized = true;
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&cond);

    pthread_join(worker, NULL);
    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond);

    printf("result = %d\n", result);

    return EXIT_SUCCESS;
}

