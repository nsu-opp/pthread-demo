#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>


typedef struct {
    int Offset;
    int Size;
    int * V1;
    int * V2;
    int * Result;

    pthread_mutex_t * Lock;
} Context;


void * vec_partial_product(void * p_context) {
    Context * context = (Context *) p_context;

    int local_product = 0;
    for (int i = context->Offset; i < context->Offset + context->Size; i += 1) {
        local_product += context->V1[i] * context->V2[i];
    }

    pthread_mutex_lock(context->Lock);
    *context->Result += local_product;
    pthread_mutex_unlock(context->Lock);

    return NULL;
}


void vec_fill(
        int * vec,
        int size,
        int value) {
    for (int i = 0; i < size; i += 1) {
        vec[i] = value;
    }
}


int main() {
    const int size = 100;
    const int threads_count = 4;

    int v1[size];
    int v2[size];
    int result = 0;

    vec_fill(v1, size, 1);
    vec_fill(v2, size, 2);

    pthread_mutex_t mutex;
    pthread_mutexattr_t mutexattrs;

    pthread_mutexattr_init(&mutexattrs);
    pthread_mutex_init(&mutex, &mutexattrs);
    pthread_mutexattr_destroy(&mutexattrs);

    Context context[threads_count];
    for (int i = 0; i < threads_count; i += 1) {
        context[i] = (Context) {
                .Offset = (size / threads_count) * i,
                .Size = size / threads_count,
                .V1 = v1,
                .V2 = v2,
                .Result = &result,
                .Lock = &mutex
        };
    }

    pthread_t workers[threads_count];
    pthread_attr_t attrs;

    pthread_attr_init(&attrs);
    pthread_attr_setdetachstate(&attrs, PTHREAD_CREATE_JOINABLE);

    for (int i = 0; i < threads_count; i++) {
        pthread_create(&workers[i], &attrs, vec_partial_product, &context[i]);
    }

    pthread_attr_destroy(&attrs);

    for (int i = 0; i < threads_count; i++) {
        pthread_join(workers[i], NULL);
    }

    pthread_mutex_destroy(&mutex);

    printf("result = %d\n", result);

    return EXIT_SUCCESS;
}

